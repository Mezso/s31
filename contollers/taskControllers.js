

const Task = require('../models/taskSchema')

//  Get all task
module.exports.getAllTasks = () =>{
	return Task.find({}).then(result =>{
		return result
	})
}
// create task
module.exports.createTask = (reqBody) => {

	let newTask = new Task({
		name: reqBody.name
	})

	return newTask.save().then((task,err) =>{
		if(err){
			console.log(err)
		}
		else{
			return task
		}
	})
}

module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndDelete(taskId).then((removedTask,err) =>{

		if(err){
			console.log(err)
			return false
		}
		else{
			return removedTask
		}
	})
}

// updated task controller

module.exports.updateTask = (taskId,newContent) =>{
	return Task.findById(taskId).then((result,err)=>{
		if(err){
			console.log(err)
			return false
		}
		result.name = newContent.name
			return result.save().then((updatedTask,saveErr) =>{
				if(saveErr){
					console.log(saveErr)
					return false
				}
				else{
					return updatedTask
				}
			})
	})
}
/*Activity Proper*/

module.exports.getTask = (taskId) =>{
	return Task.findById(taskId).then(result =>{
		return result
	})
}

module.exports.updateStatus = (taskId,newContent) =>{
	return Task.findById(taskId).then((result,err)=>{
		if(err){
			console.log(err)
			return false
		}
		result.status = newContent.status
			return result.save().then((updatedStatus,saveErr) =>{
				if(saveErr){
					console.log(saveErr)
					return false
				}
				else{
					return updatedStatus
				}
			})
	})
}